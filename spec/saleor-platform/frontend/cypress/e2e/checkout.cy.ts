import { addProductToCart } from "../lib/addProductToCart";
import { fillCheckoutForm } from "../lib/fillCheckoutFormOut";



describe("Checkout page", ()=>{
  const firstName = "Karen"
  const lastName = "Meliksetian"
  const email = "karenjantv@gmail.com"
  const address = "Schleinitzstraße 11"
  const country = "Germany"
  const postalCode = "38106"
  const city = "Braunschweig"
    it("checkout flow", () => {
        cy.visit("/products");
        cy.get('[data-testid="product"]')
          .first()
          .click();
        addProductToCart('0', '1');
        cy.visit("/checkout/information");
        fillCheckoutForm({
          firstName: firstName,
          lastName: lastName,
          email: email,
          address: address,
          country: country,
          postalCode: postalCode,
          city: city
        });
        cy.get('[data-testid="checkout-form"]').submit();
        cy.contains(email);
        cy.contains(address);
        cy.contains(city.toUpperCase());

        cy.get('[data-testid="shipping-option"]')
          .first()
          .click();
        
        cy.get('[data-testid="shipping-form"]')
          .submit();
    })
})