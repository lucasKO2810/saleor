# define GCP region
variable "gcp_region" {
  type = string
  description = "Region in EU that has Server instances"
  default = "europe-west1-b"
}

variable "gcp_region_address" {
  type = string
  description = "Region in EU that has Server instances"
  default = "europe-west1"
}

# define GCP project id
variable "gcp_project_id" {
  type = string
  description = "GCP Project id"
  default = "saleor-382911"
}

# define GCP project service account for compute engine
variable "gcp_service_account_engine" {
  type = string
  description = "GCP compute engine service account"
  default = "compute-engine@saleor-382911.iam.gserviceaccount.com"
}
