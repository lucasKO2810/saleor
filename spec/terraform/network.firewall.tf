### VPC Network ###

# create network
resource "google_compute_network" "vpc" {
  name = "vpc"
  auto_create_subnetworks = "true"
  routing_mode = "GLOBAL"
}


resource "google_compute_address" "static" {
  name = "ipv4-address"
  region = var.gcp_region_address
}

### DNS ###
resource "google_dns_managed_zone" "deep5-spec" {
  name = "deep5-spec"
  dns_name = "deep5-spec.com."
}

resource "google_dns_record_set" "spec-a-zone" {
  name         = "${google_dns_managed_zone.deep5-spec.dns_name}"
  managed_zone = google_dns_managed_zone.deep5-spec.name
  type         = "A"
  ttl          = 300
  
  rrdatas=[google_compute_address.static.address]
  
}

### Firewall Rules ###

# allow http traffic
resource "google_compute_firewall" "allow-http" {
  network = google_compute_network.vpc.name
  name = "default-allow-http"
  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  target_tags = ["http-server"]
  source_ranges = ["0.0.0.0/0"]
}
# allow https traffic
resource "google_compute_firewall" "allow-https" {
  network = google_compute_network.vpc.name
  name = "default-allow-https"
  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
  target_tags = ["https-server"]
  source_ranges = ["0.0.0.0/0"]
}
# allow https traffic
resource "google_compute_firewall" "allow-frontend" {
  network = google_compute_network.vpc.name
  name = "allow-frontend"
  allow {
    protocol = "tcp"
    ports    = ["3002"]
  }
  target_tags = ["saleor-server"]
  source_ranges = ["0.0.0.0/0"]
}

resource "google_compute_firewall" "allow-admin" {
  network = google_compute_network.vpc.name
  name = "allow-admin"
  allow {
    protocol = "tcp"
    ports    = ["9000"]
  }
  target_tags = ["saleor-server"]
  source_ranges = ["0.0.0.0/0"]
}

# allow ssh traffic
resource "google_compute_firewall" "allow-ssh" {
  network = google_compute_network.vpc.name
  name = "new-allow-ssh"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  target_tags = ["ssh-server"]
}
