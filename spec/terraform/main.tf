terraform{
  required_providers{
    google = {
    }
  }
}

provider "google" {
  credentials = "${file("saleor-terraform-cred.json")}"
  project     = var.gcp_project_id
  region      = var.gcp_region
}

data "google_compute_zones" "available_zones" {}

data "template_file" "startup_script" {
  template = "${file("./startup_script.sh")}"
  vars = {
  }
}

resource "google_compute_instance" "saleor-vm" {
  count = 1
  name = "saleor"
  machine_type = "n1-standard-1" // 4 CPU 16 GB of RAM
  zone = var.gcp_region

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      size = 50 // 50 GB Storage
    }
   }

  tags = ["https-server","http-server", "saleor-server", "ssh-server"]

  network_interface {
    network = google_compute_network.vpc.name
    access_config {
      nat_ip = google_compute_address.static.address
    }
  }

 metadata_startup_script = "${data.template_file.startup_script.rendered}"

  service_account {
  # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
  email  = var.gcp_service_account_engine
  scopes = ["cloud-platform"]
  }
}

